"""
Created on Wed Dec  1 00:05:26 2021

@author: IVAN
"""


import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

mtcars = pd.read_csv('../resources/mtcars.csv')


mtcars4 = mtcars[mtcars["cyl"] == 4]
mpg4 = mtcars4["mpg"].mean()

mtcars6 = mtcars[mtcars["cyl"] == 6]
mpg6 = mtcars6["mpg"].mean()

mtcars8 = mtcars[mtcars["cyl"] == 8]
mpg8 = mtcars8["mpg"].mean()

mpgDataCyl = {"cyl": [4, 6, 8], "mpg": [mpg4, mpg6, mpg8]}
mpgCyl = pd.DataFrame(mpgDataCyl, columns = ["cyl", "mpg"])
mpgCyl.plot.bar(x="cyl", y="mpg")

#Pomoću boxplot-a prikazane na istoj slici distribucije težine automobila s 4, 6 i 8 cilindara
wtData = {"4": mtcars4["wt"], "6": mtcars6["wt"], "8": mtcars8["wt"]}
wtCyl = pd.DataFrame(wtData, columns = ["4", "6", "8"])
wtCyl.plot.box()

#usporedba prosijecne potrosnje automobila s rucnim i automatskim mijenjacem
mtcarsA = mtcars[mtcars["am"] == 0]
mtcarsR = mtcars[mtcars["am"] == 1]

mpgA = mtcarsA["mpg"].mean()
mpgR = mtcarsR["mpg"].mean()

mpgDataAm = {"am": [0, 1], "mpg": [mpgA, mpgR]}
mpgAm = pd.DataFrame(mpgDataAm, columns = ["am", "mpg"])
mpgAm.plot.bar(x = "am", y = "mpg")

#odnos snage i ubrzanja
colors = np.where(mtcars["am"]==1,'r','b')
mtcars.plot.scatter(x="hp", y="qsec", c = colors)
plt.legend("1")