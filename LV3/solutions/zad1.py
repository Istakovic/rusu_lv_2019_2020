# -*- coding: utf-8 -*-
"""
Created on Wed Dec  1 00:05:26 2021

@author: IVAN
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

mtcars = pd.read_csv('../resources/mtcars.csv')

#5 auta s najvecom potrosnjom
mtcars_mpg = mtcars.sort_values("mpg", ascending = False)
print("5 auta s najvecom potrosnjom:\n")
print(mtcars_mpg[["car"]].head(5))
print("\n\n")

#auti s 8 cilindara koji imaju najmanju potrošnju
mtcars_cyl8 = mtcars[mtcars["cyl"] == 8]
mtcars_cyl8 = mtcars_cyl8.sort_values("mpg", ascending = True)
print("auti s 8 cilindara koji imaju najmanju potrosnju:\n")
print(mtcars_cyl8[["car"]].head(8))
print("\n\n")

#srednja potrosnja automobila sa 6 cilindara
mtcars_cyl6 = mtcars[mtcars["cyl"] == 6]
print("srednja potrosnja automobila sa 6 cilindara:\n")
print(mtcars_cyl6["mpg"].mean())
print("\n\n")

#srednja potrosnja automobila s 4 cilindra i tezinom izmedu 2000 i 2200 lbs
mtcars_cyl4 = mtcars[mtcars["cyl"] == 4]
mtcars_cyl4 = mtcars_cyl4[mtcars_cyl4["wt"] >= 2.0]
mtcars_cyl4 = mtcars_cyl4[mtcars_cyl4["wt"] <= 2.2]
print("srednja potrosnja automobila s 4 cilindra i tezinom izmedu 2000 i 2200 lbs:\n")
print(mtcars_cyl4["mpg"].mean())
print("\n\n")

#broj automobila s automatskim i rucnim mjenjacem
mtcars_a = mtcars[mtcars["am"] == 0]
mtcars_r = mtcars[mtcars["am"] == 1]
print("broj automobila s automatskim i rucnim mjenjacem:\n")
print(len(mtcars_a))
print(len(mtcars_r))
print("\n\n")

#broj automobila s automatskim mjenjacem i snagom preko 100 konja
mtcars_a = mtcars_a[mtcars_a["hp"] > 100]
print("broj automobila s automatskim mjenjacem preko 100 konja:\n")
print(len(mtcars_a))
print("\n\n")

#masa svakog automobila u kilogramima
mtcars_kg = mtcars.copy()
mtcars_kg["wt"] = mtcars_kg["wt"] * 0.45359 * 1000
print("masa svakog automobila u kilogramima\n")
print(mtcars_kg[["car", "wt"]])