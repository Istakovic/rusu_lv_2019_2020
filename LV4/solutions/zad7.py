
import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
import sklearn.metrics as sm
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import PolynomialFeatures
from sklearn import preprocessing


def generate_data(n):
	
	#prva klasa
	n1 = n/2
	x1_1 = np.random.normal(0.0, 2, (n1,1));
	#x1_1 = .21*(6.*np.random.standard_normal((n1,1)));
	x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1));
	y_1 = np.zeros([n1,1])
	temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)

	#druga klasa
	n2 = n - n/2
	x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2);
	y_2 = np.ones([n2,1])
	temp2 = np.concatenate((x_2,y_2),axis = 1)
	data = np.concatenate((temp1,temp2),axis = 0)

	#permutiraj podatke
	indices = np.random.permutation(n)
	data = data[indices,:]

	return data #matrica s 3 stupca(2 za ulazne vrijednosti i 1 za izlaz)
np.random.seed(242)  #242 kako bi stalno ggenerirao iste brojeve
data_train=generate_data(200) #generiramo skup za učenje veličine 200 uzoraka

np.random.seed(12)  #12 kako bi stalno ggenerirao iste brojeve
data_test=generate_data(100) #generiramo skup za testiranje veličine 100 uzoraka uzoraka 


plt.figure()
for i in range (0,len(data_train)):
    if(data_train[i,2]) == 0:
        color="blue"
    else:
        color="red"
    plt.scatter(data_train[i,0],data_train[i,1],c=color)
plt.title('Granica odluke naucenog modela s podacima za ucenje')
plt.xlabel('x1')
plt.ylabel('x2')
# fit(X, y, sample_weight=None)[source]
model = lm.LogisticRegression()
model.fit(data_train[:,:-1], data_train[:,2]) #fit(X,y), [:,:-1] --svi redovi i svi stupci minus zadnji
                                                  #      [:,2]- svi redovi, samo 3. stupac

#granica odluke : theta0+theta1*X1+theta2*X2 = 0
#granica odluke : X2=-(theta0+theta1*X1)/theta2
X2=-(model.intercept_ + model.coef_[0,0]*data_train[:,0])/model.coef_[0,1] #X2= -(theta0+theta1*X1)/theta2


f, ax = plt.subplots(figsize=(8, 6))
x_grid, y_grid = np.mgrid[min(data_train[:,0])-0.5:max(data_train[:,0])+0.5:.05,min(data_train[:,1])-0.5:max(data_train[:,1])+0.5:.05]
grid = np.c_[x_grid.ravel(), y_grid.ravel()]

for i in range (0,len(data_train)):
    if(data_train[i,2]) == 0:
        color="blue"
    else:
        color="red"
    ax.scatter(data_train[i,0],data_train[i,1],c=color, zorder=2) #uzorci, zorder=2 -->da bude ispred pozadine
    
ax.plot(data_train[:,0],X2)  #pravac

probs = model.predict_proba(grid)[:, 1].reshape(x_grid.shape)
cont = ax.contourf(x_grid, y_grid, probs, 60, cmap="Greys", vmin=0, vmax=1)
ax_c = f.colorbar(cont)
ax_c.set_label("$P(y = 1|\mathbf{x})$")
ax_c.set_ticks([0, .25, .5, .75, 1])
ax.set_xlabel('$x_1$', alpha=0.9)
ax.set_ylabel('$x_2$', alpha=0.9)
ax.set_title('Izlaz logisticke regresije')
plt.show()

y_predict=model.predict(data_test[:,:-1])     #predict(X)  -X = ulazi(1. i 2. stupac)

plt.figure()
for i in range (0,len(data_test)):
    if(data_test[i,2]) == y_predict[i]:
        color="green"  #ispravno klasificirani podaci
    else:
        color="black"  #pogresno klasificirani podaci
    plt.scatter(data_test[i,0],data_test[i,1],c=color)
plt.title('Zadatak 5- Klasifikacija testnog skupa')
plt.xlabel('x1')
plt.ylabel('x2')


def plot_confusion_matrix(c_matrix):

	norm_conf = []
	for i in c_matrix:
		a = 0
		tmp_arr = []
		a = sum(i, 0)
		for j in i:
			tmp_arr.append(float(j)/float(a))
		norm_conf.append(tmp_arr)
	
	fig = plt.figure()
	ax = fig.add_subplot(111)
	res = ax.imshow(np.array(norm_conf), cmap=plt.cm.Greys, interpolation='nearest')
	
	width = len(c_matrix)
	height = len(c_matrix[0])

	for x in xrange(width):
		for y in xrange(height):
			ax.annotate(str(c_matrix[x][y]), xy=(y, x),horizontalalignment='center',verticalalignment='center', color = 'green', size = 20)

	fig.colorbar(res)
	numbers = '0123456789'
	plt.xticks(range(width), numbers[:width])
	plt.yticks(range(height), numbers[:height])
	plt.ylabel('Stvarna klasa')
	plt.title('Predvideno modelom')
	plt.show()
    
""" moj kod"""
matrica_cm=sm.confusion_matrix(data_test[:,2],y_predict[:])  #matrica zabune
plot_confusion_matrix(matrica_cm)  #poziv funkcije za crtanje matrice zabune

TP=float(matrica_cm[0,0]) #int u float kako bismo mogli dobiti decimalni rezultat
TN=float(matrica_cm[1,1]) #kad izračunavano pokazatelje
FP=float(matrica_cm[0,1])
FN=float(matrica_cm[1,0])

#pokakzatelji klasifikacijskog modela
accuracy=(TP+TN)/(TP+TN+FP+FN)*100
missclasification_rate=(1-accuracy)*100
precision=TP/(TP+FP)*100
sensitivity=TP/(TP+FN)*100
specificity=TN/(TN+FP)*100

poly = PolynomialFeatures(degree=3, include_bias = False)
data_train_new = poly.fit_transform(data_train[:,0:2])


# fit(X, y)
model2 = lm.LogisticRegression()
model2.fit(data_train_new[:,:-1], data_train[:,2]) #fit(X,y), [:,:-1] --svi redovi i svi stupci minus zadnji
                                                  #      [:,2]- svi redovi, samo 3. stupac
#granica odluke : theta0+theta1*X1+theta2*X2 = 0
#granica odluke : X2=-(theta0+theta1*X1)/theta2
X2=-(model2.intercept_ + model2.coef_[0,0]*data_train[:,0])/model2.coef_[0,1] #X2= -(theta0+theta1*X1)/theta2
#plt.plot(data_train[:,0],X2)