# -*- coding: utf-8 -*-
"""
Created on Sun Dec 12 20:31:25 2021

@author: IVAN
"""
import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
import sklearn.metrics as sm
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import PolynomialFeatures
from sklearn import preprocessing


def generate_data(n):
	
	#prva klasa
	n1 = n/2
	x1_1 = np.random.normal(0.0, 2, (n1,1));
	#x1_1 = .21*(6.*np.random.standard_normal((n1,1)));
	x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1));
	y_1 = np.zeros([n1,1])
	temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)

	#druga klasa
	n2 = n - n/2
	x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2);
	y_2 = np.ones([n2,1])
	temp2 = np.concatenate((x_2,y_2),axis = 1)
	data = np.concatenate((temp1,temp2),axis = 0)

	#permutiraj podatke
	indices = np.random.permutation(n)
	data = data[indices,:]

	return data #matrica s 3 stupca(2 za ulazne vrijednosti i 1 za izlaz)
np.random.seed(242)  #242 kako bi stalno ggenerirao iste brojeve
data_train=generate_data(200) #generiramo skup za učenje veličine 200 uzoraka

np.random.seed(12)  #12 kako bi stalno ggenerirao iste brojeve
data_test=generate_data(100) #generiramo skup za testiranje veličine 100 uzoraka uzoraka 

