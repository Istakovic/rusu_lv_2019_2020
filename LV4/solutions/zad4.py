
import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
import sklearn.metrics as sm
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import PolynomialFeatures
from sklearn import preprocessing


def generate_data(n):
	
	#prva klasa
	n1 = n/2
	x1_1 = np.random.normal(0.0, 2, (n1,1));
	#x1_1 = .21*(6.*np.random.standard_normal((n1,1)));
	x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1));
	y_1 = np.zeros([n1,1])
	temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)

	#druga klasa
	n2 = n - n/2
	x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2);
	y_2 = np.ones([n2,1])
	temp2 = np.concatenate((x_2,y_2),axis = 1)
	data = np.concatenate((temp1,temp2),axis = 0)

	#permutiraj podatke
	indices = np.random.permutation(n)
	data = data[indices,:]

	return data #matrica s 3 stupca(2 za ulazne vrijednosti i 1 za izlaz)
np.random.seed(242)  #242 kako bi stalno ggenerirao iste brojeve
data_train=generate_data(200) #generiramo skup za učenje veličine 200 uzoraka

np.random.seed(12)  #12 kako bi stalno ggenerirao iste brojeve
data_test=generate_data(100) #generiramo skup za testiranje veličine 100 uzoraka uzoraka 


plt.figure()
for i in range (0,len(data_train)):
    if(data_train[i,2]) == 0:
        color="blue"
    else:
        color="red"
    plt.scatter(data_train[i,0],data_train[i,1],c=color)
plt.title('Granica odluke naucenog modela s podacima za ucenje')
plt.xlabel('x1')
plt.ylabel('x2')
# fit(X, y, sample_weight=None)[source]
model = lm.LogisticRegression()
model.fit(data_train[:,:-1], data_train[:,2]) #fit(X,y), [:,:-1] --svi redovi i svi stupci minus zadnji
                                                  #      [:,2]- svi redovi, samo 3. stupac

#granica odluke : theta0+theta1*X1+theta2*X2 = 0
#granica odluke : X2=-(theta0+theta1*X1)/theta2
X2=-(model.intercept_ + model.coef_[0,0]*data_train[:,0])/model.coef_[0,1] #X2= -(theta0+theta1*X1)/theta2


f, ax = plt.subplots(figsize=(8, 6))
x_grid, y_grid = np.mgrid[min(data_train[:,0])-0.5:max(data_train[:,0])+0.5:.05,min(data_train[:,1])-0.5:max(data_train[:,1])+0.5:.05]
grid = np.c_[x_grid.ravel(), y_grid.ravel()]

for i in range (0,len(data_train)):
    if(data_train[i,2]) == 0:
        color="blue"
    else:
        color="red"
    ax.scatter(data_train[i,0],data_train[i,1],c=color, zorder=2) #uzorci, zorder=2 -->da bude ispred pozadine
    
ax.plot(data_train[:,0],X2)  #pravac

probs = model.predict_proba(grid)[:, 1].reshape(x_grid.shape)
cont = ax.contourf(x_grid, y_grid, probs, 60, cmap="Greys", vmin=0, vmax=1)
ax_c = f.colorbar(cont)
ax_c.set_label("$P(y = 1|\mathbf{x})$")
ax_c.set_ticks([0, .25, .5, .75, 1])
ax.set_xlabel('$x_1$', alpha=0.9)
ax.set_ylabel('$x_2$', alpha=0.9)
ax.set_title('Izlaz logisticke regresije')
plt.show()
