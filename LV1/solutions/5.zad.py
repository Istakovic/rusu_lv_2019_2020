confidences = []

fname = input('Enter the file name: ')  
try:
    fhand = open(fname)
except:
    print ('File cannot be opened:', fname)
    exit()

for line in fhand:
    if(line.startswith("X-DSPAM-Confidence:")):
        linePart=line.split()
        confidences.append(float(linePart[1]))
       
print("Ime datoteke",fname)
print("Average X-DSPAM-Confidence:",sum(confidences)/len(confidences))