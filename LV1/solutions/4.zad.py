brojevi = []
ulaz = 0

while(ulaz != "Done"):
    try:
        ulaz = input("Unesite broj ili Done ako ste gotovi: ")

        brojevi.append(float(ulaz))

    except:
        print("Niste unijeli broj")

print("Length: ", len(brojevi),"\nSrednja vrijednost: ", sum(brojevi)/len(brojevi), "\nMax: ", max(brojevi), "\nMin: ", min(brojevi))