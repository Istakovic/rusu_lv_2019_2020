# -*- coding: utf-8 -*-
"""
Created on Mon Nov 22 16:03:06 2021

@author: IVAN
"""

import pandas as pnd
import numpy as np
import matplotlib.pyplot as plt


fig = plt.figure()
axis = fig.add_subplot(1,1,1)

CarsData = pnd.read_csv("../resources/mtcars.csv")

plt.scatter(CarsData.mpg,CarsData.hp)
plt.xlabel("Miles Per Gallon")
plt.ylabel("Horse Power")
plt.grid(linestyle="--")

ReadingArray = np.array(CarsData.mpg,float)

for i in range (0, len(CarsData.mpg)):       
    axis.annotate('%.2f' % CarsData.wt[i],xy=(CarsData.mpg[i], CarsData.hp[i]), 
                xytext=(CarsData.mpg[i]+3, CarsData.hp[i]-20), 
                arrowprops=dict(arrowstyle="-", facecolor='red'), 
                )

print("Maksimalna vrijednost potrošnje goriva je: "+ str(ReadingArray.max()))
print("Minimalna vrijednost potrošnje goriva je: "+ str(ReadingArray.min()))
print("Srednja vrijednost potrošnje goriva je: "+ str(ReadingArray.mean()))
