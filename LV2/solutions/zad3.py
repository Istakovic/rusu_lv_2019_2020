# -*- coding: utf-8 -*-
"""
Created on Mon Nov 22 15:58:17 2021

@author: IVAN
"""

import numpy as np                    
import matplotlib.pyplot as plt       

niz1 = np.random.randint(2, size=100) #Random 0,1
niz_gauss = np.random.randint(1, size=100)  #Sve 0  

prikaz_m=[]     
prikaz_z=[]     

for i in range (0, 100):
    if niz1[i]==1:      
        niz_gauss[i] = np.random.normal(180, 7, None)   
                                                
    else:                                       
        niz_gauss[i] = np.random.normal(167, 7, None)   
        
for i in range (0, 100):
    if niz1[i]==1:     
        prikaz_m.append(niz_gauss[i])   
    else:
        prikaz_z.append(niz_gauss[i])   

plt.hist([prikaz_m, prikaz_z], color=['blue','red']); # plava/muškarci, crveno/žene
plt.legend(["Muskarci","Zene"])                       

n1=np.average(prikaz_m)   #izračun prosječne vrijednosti za visinu muškaraca
n2=np.average(prikaz_z)   #izračun prosječne vrijednosti za visinu žena

plt.axvline(n1, color='chocolate', linestyle=':', linewidth=2)  
plt.axvline(n2, color='grey', linestyle='--', linewidth=2)  