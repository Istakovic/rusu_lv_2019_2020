# -*- coding: utf-8 -*-
"""
Created on Mon Nov 22 15:53:45 2021

@author: IVAN
"""
import re

def AtleastOneA(ispred):
    for n in range(0,len(ispred),1):
        if re.match(".*a+.*",ispred[n]):
            print ("Prvi dio mail adrese: " + ispred[n]) 
            
def ExactlyOneA(ispred):
    for n in range(0,len(ispred),1):
        if re.fullmatch("[^a]*a[^a]*",ispred[n]):
            print ("Prvi dio mail adrese: " + ispred[n]) 
            
def WithoutA(ispred):
    for n in range(0,len(ispred),1):
        if not(re.match(".*a+.*",ispred[n])):
            print ("Prvi dio mail adrese: " + ispred[n])
            
def WithNumbers(ispred):
    for n in range(0,len(ispred),1):
        if re.match("[0-9]",ispred[n]):
            print ("Prvi dio mail adrese: " + ispred[n])
            
def OnlySmallLetters(ispred):
    for n in range(0,len(ispred),1):
        if re.match("[a-z]",ispred[n]):
            print ("Prvi dio mail adrese: " + ispred[n])




fhand = open("../resources/mbox-short.txt")
i=0;  
e_mail = []
ispred = []

choice = input("Unesite broj 1-5: ")

for line in fhand:
    line =line.rstrip() 
    mail=re.search(r'[\w\.-]+@[\w\.-]+', line)
    if mail != None : 
        e_mail.append(mail.group(0)) 
for n in range(0,len(e_mail),1):
    ispred.append(e_mail[n].split("@")[0])  
    
if choice == "1":
    AtleastOneA(ispred)
elif choice == "2":
    ExactlyOneA(ispred)
elif choice == "3":
    WithoutA(ispred)
elif choice == "4":
    WithNumbers(ispred)
else:
    OnlySmallLetters(ispred)
