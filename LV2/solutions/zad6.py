# -*- coding: utf-8 -*-
"""
Created on Mon Nov 22 16:03:57 2021

@author: IVAN
"""

import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np
brightness = int(input("Input the level of brightness increase (1-100): "))
img = mpimg.imread("../resources/tiger.png")
imgplot = plt.imshow(img)
matricabojanja = np.array(img)
matricabojanja = matricabojanja * brightness
matricabojanja = np.clip(matricabojanja,0,1)    
imgnew = plt.imshow(matricabojanja)